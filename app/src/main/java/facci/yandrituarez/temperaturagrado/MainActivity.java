package facci.yandrituarez.temperaturagrado;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.*;
import android.widget.TextView;


public class MainActivity extends Activity {

    CheckBox CheckBoxCaF, CheckBoxFaC;
    EditText EditTextnumero;
    TextView textViewresultado;
    Button btnconversion;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditTextnumero = (EditText) findViewById(R.id.EditTextnumero);
        CheckBoxCaF = (CheckBox) findViewById(R.id.CheckBoxCaF);
        CheckBoxFaC = (CheckBox) findViewById(R.id.CheckBoxFaC);
        textViewresultado = (TextView) findViewById(R.id.textViewresultado);
        btnconversion = (Button) findViewById(R.id.btnconversion);

        btnconversion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 if(EditTextnumero.getText().toString().isEmpty()){
                     Toast.makeText(getApplicationContext(), "Debes ingresar un numero",Toast.LENGTH_SHORT);
                 }else if(CheckBoxCaF.isChecked ()) {
                double cantidad = Double.parseDouble(EditTextnumero.getText().toString());
                float resultadoF = (float) ((cantidad * 1.8) + 32);
                    textViewresultado.setText(String.valueOf(resultadoF+ " °F"));
                }else if(CheckBoxFaC.isChecked ()){
                    double cantidad = Double.parseDouble(EditTextnumero.getText().toString());
                    float resultadoC = (float) ((cantidad - 32)/(1.8));
                    textViewresultado.setText(String.valueOf(resultadoC+ " °C"));
                 } else if (CheckBoxCaF.isChecked ()|| CheckBoxFaC.isChecked ()) {
                    Toast.makeText(getApplicationContext(), "Error, haz seleccionado todos los casilleros", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getApplicationContext(), "Seleccione un casillero", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
}
